package com.gitee.sqlrest.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.sqlrest.persistence.entity.AppClientEntity;

public interface AppClientMapper extends BaseMapper<AppClientEntity> {

}
