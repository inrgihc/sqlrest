package com.gitee.sqlrest.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.sqlrest.persistence.entity.SystemParamEntity;

public interface SystemParamMapper extends BaseMapper<SystemParamEntity> {

}
