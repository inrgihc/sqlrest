package com.gitee.sqlrest.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.sqlrest.persistence.entity.ApiGroupEntity;

public interface ApiGroupMapper extends BaseMapper<ApiGroupEntity> {

}
