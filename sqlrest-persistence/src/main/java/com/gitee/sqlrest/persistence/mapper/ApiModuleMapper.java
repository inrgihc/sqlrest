package com.gitee.sqlrest.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.sqlrest.persistence.entity.ApiModuleEntity;

public interface ApiModuleMapper extends BaseMapper<ApiModuleEntity> {

}
