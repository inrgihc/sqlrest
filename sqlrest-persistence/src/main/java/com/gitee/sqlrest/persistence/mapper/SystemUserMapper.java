package com.gitee.sqlrest.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.sqlrest.persistence.entity.SystemUserEntity;

public interface SystemUserMapper extends BaseMapper<SystemUserEntity> {

}
