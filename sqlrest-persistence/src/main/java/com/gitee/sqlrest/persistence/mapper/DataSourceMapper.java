package com.gitee.sqlrest.persistence.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.sqlrest.persistence.entity.DataSourceEntity;

public interface DataSourceMapper extends BaseMapper<DataSourceEntity> {

}
