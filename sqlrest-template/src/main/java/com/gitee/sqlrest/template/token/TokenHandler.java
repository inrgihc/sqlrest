package com.gitee.sqlrest.template.token;

public interface TokenHandler {

  String handleToken(String content);
}
