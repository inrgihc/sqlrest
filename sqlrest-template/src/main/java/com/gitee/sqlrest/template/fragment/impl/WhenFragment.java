package com.gitee.sqlrest.template.fragment.impl;

import com.gitee.sqlrest.template.fragment.SqlFragment;

public class WhenFragment extends IfFragment {

  public WhenFragment(SqlFragment contents, String test) {
    super(contents, test);
  }

}
